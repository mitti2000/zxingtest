﻿namespace ZXingTest
{
    using Prism.Commands;
    using Prism.Navigation;

    using Xamarin.Forms;

    using ZXing;
    using ZXing.Mobile;

    public class ScannerViewModel : ViewModelBase
    {
        private string _bottomText;

        private bool _isAnalyzing = true;

        private bool _isScanning;

        private Result _result;

        private MobileBarcodeScanningOptions _scanningOptions;

        private string _topText;

        public ScannerViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            var options = new MobileBarcodeScanningOptions();
            options.TryHarder = true;
            options.InitialDelayBeforeAnalyzingFrames = 300;
            options.DelayBetweenContinuousScans = 100;
            options.DelayBetweenAnalyzingFrames = 200;
            options.AutoRotate = false;

            ScanningOptions = options;
            Title = "Barcode-Scanner";
            CmdScanResult = new DelegateCommand(OnCmdScanResult);
            TopText = "My Barcodescanner";
            BottomText = "FooterText";
            IsScanning = true;
            IsAnalyzing = true;
        }

        public MobileBarcodeScanningOptions ScanningOptions
        {
            get => _scanningOptions;

            set => SetProperty(ref _scanningOptions, value);
        }

        public bool IsScanning
        {
            get => _isScanning;

            set => SetProperty(ref _isScanning, value);
        }

        public bool IsAnalyzing
        {
            get => _isAnalyzing;

            set => SetProperty(ref _isAnalyzing, value);
        }

        public Result Result
        {
            get => _result;

            set => SetProperty(ref _result, value);
        }

        public DelegateCommand CmdScanResult { get; }

        public string TopText
        {
            get => _topText;

            set => SetProperty(ref _topText, value);
        }

        public string BottomText
        {
            get => _bottomText;

            set => SetProperty(ref _bottomText, value);
        }

        private void OnCmdScanResult()
        {
            IsAnalyzing = false;
            IsScanning = false;
            Device.BeginInvokeOnMainThread(
                async () =>
                    {
                        IsAnalyzing = false;
                        IsScanning = false;

                        var parameters = new NavigationParameters();
                        parameters.Add("Barcode", Result);
                        await NavigationService.GoBackAsync(parameters);
                    });
        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            IsScanning = true;
            IsAnalyzing = true;
            if (parameters.Count == 0)
            {
                parameters.Add("Barcode", null);
            }
        }
    }
}