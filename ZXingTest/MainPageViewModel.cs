﻿namespace ZXingTest
{
    using Prism.Commands;
    using Prism.Navigation;

    public class MainPageViewModel : ViewModelBase
    {
        private string _buttonText;
        private string _scanResult;

        public MainPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            Title = "Main Page";
            CmdScanButtonClicked = new DelegateCommand(OnScanButtonClicked);
            ButtonText = "Scan Barcode";
        }

        public string ButtonText
        {
            get => _buttonText;
            set => SetProperty(ref _buttonText, value);
        }

        public string ScanResult
        {
            get => _scanResult;
            set => SetProperty(ref _scanResult, value);
        }

        public DelegateCommand CmdScanButtonClicked { get; }

        private void OnScanButtonClicked()
        {
            NavigationService.NavigateAsync("ScannerView");
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if(parameters.Count == 0)
            {
                return;
            }

            if (parameters.ContainsKey("Barcode"))
            {
                if (parameters["Barcode"] != null)
                {
                    ScanResult = parameters["Barcode"].ToString();
                }
            }
        }
    }
}