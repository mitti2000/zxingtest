﻿namespace ZXingTest
{
    using Xamarin.Forms.Xaml;

    using ZXing.Net.Mobile.Forms;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ScannerView : ZXingScannerPage
    {
        public ScannerView()
        {
            InitializeComponent();
        }
    }
}